
const elements = require('../../data/elements.json');

const type = {
    AlkaliMetal: 'AlkaliMetal',
    AlkalineEarthMetal: 'AlkalineEarthMetal',
    Lanthanide: 'Lanthanide', 
    Actinide: 'Actinide', 
    TransitionMetal: 'TransitionMetal',
    PostTransitionMetal: 'PostTransitionMetal',
    Metalloid: 'Metalloid',
    ReactiveNonMetal: 'ReactiveNonMetal',
    NobleGas: 'NobleGas',
    }

const createElements = () => {
    let elementObjects = [];
    elements.forEach(e => {
        elementObjects.push(createElement(e));
    });
    elementObjects.push(createReference(1));
    elementObjects.push(createReference(2));
    return elementObjects;
}
const createReference = (number) => {
    const referenceElement = document.createElement('h3');
    referenceElement.classList.add('reference');
    if(number === 1) {
        referenceElement.innerText = "*";
        referenceElement.classList.add('ref1');
    } else if (number === 2) {
        referenceElement.innerText = "*\n*"
        referenceElement.classList.add('ref2');
    }
    return referenceElement;
}
const createElement = (elementInfo) => {
    const element = document.createElement('DIV');
    element.classList.add('element'); 
    element.classList.add(elementInfo.symbol); 
    element.classList.add(type[elementInfo.type]); 
    

    element.appendChild(createName(elementInfo.name));
    element.appendChild(createInfoWrapper(elementInfo.id, elementInfo.symbol));

    return element;
}

const createInfoWrapper = (id, symbol) => {
    const elementInfoWrapperObj = document.createElement('DIV');
    elementInfoWrapperObj.classList.add("wrapper");
    elementInfoWrapperObj.appendChild(createNumber(id));
    elementInfoWrapperObj.appendChild(createSymbol(symbol));
    return elementInfoWrapperObj;
}

const createName = (elementName) => {
    const elementNameObj = document.createElement('H6');
    elementNameObj.innerText = elementName;
    elementNameObj.classList.add('name');
    return elementNameObj;
}

const createNumber = (elementId) => {
    const elementIdObj = document.createElement('H6');
    elementIdObj.innerText = elementId;
    elementIdObj.classList.add('id');
    return elementIdObj;
}

const createSymbol = (elementSymbol) => {
    const elementSymbolObj = document.createElement('H6');
    elementSymbolObj.innerText = elementSymbol;
    elementSymbolObj.classList.add('symbol');
    return elementSymbolObj;
}

export {createElements}