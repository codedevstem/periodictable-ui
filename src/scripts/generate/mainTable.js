
import {createElements} from './elements'

const generateMainTable = () => {
    const main = document.createElement("DIV");
    main.classList.add('mainTable');
    createElements().forEach(e => {
        main.appendChild(e);
    });
    return main;
};


export {generateMainTable};
