
import '../styles/elements.css';
import '../styles/tables.css';
import '../styles/master.css';

import { generateMainTable } from './generate/mainTable';

window.onload = () => {
    document.body.appendChild(generateMainTable());
};

