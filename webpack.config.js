const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');


const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
    mode: 'development',
    entry: './src/scripts/master.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist'
    },
    plugins: [
        new CleanWebpackPlugin(['dist/*']),
        new HtmlWebpackPlugin({
            title: 'Periodic Table Explorer'
        })
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            }
        ],
        loaders: [
            {
              test: /\.json$/,
              loader: 'json-loader'
            }
          ]
    },
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000
    }
};